import json
import urllib2

key = "trnsl.1.1.20150127T220525Z.fb42a76b2a2f1e97.a1ff94303180b250c21156e50dad56e2fe2d6e29"

def sanit(text):
	s = str()
	for i in text:
		i = ord(i)
		if (i>=65 and i<= 90) or (i>=97 and i<=122) or (i==44) or (i==46):
			s += chr(i)
		elif (i==195) or (i==129) or (i==137) or (i==141) or (i==145) or (i==147) or (i==154) or (i==169) or (i==173) or (i==177) or (i==179) or (i==186):
			s += chr(i)
		elif i==32:
			s += '+'
	return s

sub = "worldnews"
fr = "en"
to = "es"
response = urllib2.urlopen( "https://www.reddit.com/r/" + sub + ".json" ).read()
data = json.loads(response)
stories = []
for story in data["data"]["children"]:
    url = "https://translate.yandex.net/api/v1.5/tr.json/translate?key="+key+"&lang="+fr+"-"+to+"&text="+sanit(story["data"]["title"])
    response = urllib2.urlopen( url ).read()
    data = json.loads(response)
    title = data["text"][0]
    stories.append( [ story["data"]["id"], title, story["data"]["num_comments"], story["data"]["url"] ] )

print(stories)