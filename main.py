import os
import json
import jinja2
import webapp2
import urllib
import urllib2

from google.appengine.api import urlfetch
urlfetch.set_default_fetch_deadline(60)

key = "trnsl.1.1.20150127T220525Z.fb42a76b2a2f1e97.a1ff94303180b250c21156e50dad56e2fe2d6e29"

def sanit(text):
	s = str()
	o = [32,44,46,129,137,141,147,154,161,169,173,177,179,186,195]
	for i in text:
		n = ord(i)
		if (n>=65 and n<= 90) or (n>=97 and n<=122) or (n in o):
			s += i
	return s

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__),'templates')),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write('Welcome to REDLATE! ')
        self.response.write(' Please enter the following links to be translated: worldnews, sports, television, funny, futurology, gaming,random, etc...')
        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render())

class SubHandler(webapp2.RequestHandler):
    def get(self):
        sub = self.request.get("sub")
        fr = self.request.get("from")
        to = self.request.get("to")
        response = urllib2.urlopen( "https://www.reddit.com/r/" + sub + ".json" ).read()
        data = json.loads(response)
        stories = []
        for story in data["data"]["children"]:
            params = {'key': key, 'lang': fr+"-"+to, 'text': sanit(story["data"]["title"])}
            url = "https://translate.yandex.net/api/v1.5/tr.json/translate"
            data = urllib.urlencode(params)
            response = urllib2.urlopen(url,data,180)
            data = json.load(response)
            title = data["text"][0]
            stories.append( [ story["data"]["id"], title, story["data"]["num_comments"], story["data"]["url"] ] )
        print(stories)
        if (to=="en"):
            lang = "en-gb"
        else:
            lang = to+"-"+to
        templates_values = {'stories': stories, 'sub':sub, 'from': fr, 'to':to, 'lang':lang}
        template = JINJA_ENVIRONMENT.get_template('subreddit.html')
        self.response.write(template.render(templates_values))

class ComHandler(webapp2.RequestHandler):
    def get(self):
        fr = self.request.get("from")
        to = self.request.get("to")
        lang = self.request.get("lang")
        response = urllib2.urlopen( "https://www.reddit.com/r/" + self.request.get("sub") + "/comments/" + self.request.get("story") + ".json" ).read()
        data = json.loads(response)
        comments = []
        for y in data:
            for x in y["data"]["children"]:
                try:
                    params = {'key': key, 'lang': fr+"-"+to, 'text': sanit(x["data"]["body"])}
                    url = "https://translate.yandex.net/api/v1.5/tr.json/translate"
                    data = urllib.urlencode(params)
                    response = urllib2.urlopen(url,data,180)
                    data = json.load(response)
                    body = data["text"][0]
                    comments.append([ body, x["data"]["author"] ])
                except:
                    print "er"

        templates_values = { 'comments': comments, 'lang':lang }

        template = JINJA_ENVIRONMENT.get_template('comments.html')
        self.response.write(template.render(templates_values))

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/topic', SubHandler),
    ('/comments', ComHandler)
], debug=True)
